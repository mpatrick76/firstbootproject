yieldUnescaped '<!DOCTYPE html>'
html {
	body {
		h2("Notes")

		form (action: "/addnote") {
		yield "Add Another: "
			input(type: 'text', name: 'noteText')
			input(type: 'submit', value: 'Add Note')
			input(type: 'hidden', name: 'login', value: "${username}")
		}
		br{}
		a(href: "/Dashboard?login=${username}", "Back to Dashboard")
		br{}
		if (notes) {
			notes.each { note ->
				yield note
				br {}
			}
		}
		
	}
}