yieldUnescaped '<!DOCTYPE html>'
html {
	body {
		h2("Welcome, ${username}!")
		form (action: '/addnote') {
			input(type: 'text', name: 'noteText')
			input(type: 'hidden', name: 'login', value: "${username}")
			input(type: 'submit', value: 'Add a note')
		}
		br{}
		a(href: "/deleteAllNotes?login=${username}", "Delete all notes") 
		br{}
		a(href: "/manageNotes?login=${username}", "Manage notes")
		br{}
		if (notes) {
			notes.each { note ->
				yield note
				br {}
			}
		}
	}
}