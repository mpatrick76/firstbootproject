package com.cds.spring.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.groovy.GroovyMarkupConfigurer;
import org.springframework.web.servlet.view.groovy.GroovyMarkupViewResolver;

@Configuration
@SpringBootApplication
@EnableWebMvc
public class ApplicationRunner  {//extends SpringBootServletInitializer {//WebMvcConfigurerAdapter {

	public static void main(String... args) {
		ApplicationContext ctx = SpringApplication.run(ApplicationRunner.class, args);
	}
	
    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("WEB-INF/jsp/");
        resolver.setSuffix(".jsp");
        resolver.setOrder(1);
        return resolver;
    }
    
    @Bean
    public GroovyMarkupConfigurer groovyMarkupConfigurer() {
    	GroovyMarkupConfigurer configurer = new GroovyMarkupConfigurer();
    	configurer.setResourceLoaderPath("/WEB-INF/pages/groovy/");
    	return configurer;
		//return new GroovyMarkupConfigurer( [resourceLoaderPath: "/WEB-INF/pages/groovy/"] )
    }
	
	@Bean public GroovyMarkupViewResolver groovyViewResolver() {
		GroovyMarkupViewResolver resolver = new GroovyMarkupViewResolver();
		resolver.setSuffix(".groovy");
		resolver.setOrder(0);
		return resolver;
		//return new GroovyMarkupViewResolver( [suffix: ".groovy", order: 1] )
	}

}
