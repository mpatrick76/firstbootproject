package com.cds.spring.training.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EntryController {

	final static Logger LOG = LogManager.getLogger(EntryController.class);
	
	@Autowired HttpServletRequest request;
	
	@RequestMapping("/Enter")
	public ModelAndView applicationEntry(@RequestParam(value="viewType", required=false) String viewType) {
		LOG.info("Entry controller method");
		if ("groovy".equals(viewType)) {
			return new ModelAndView("welcome");
		} else {
			return new ModelAndView("welcome-jsp");
		}
	}
	
	@RequestMapping("/Dashboard")
	public ModelAndView dashboard(@RequestParam String login) {
		List<String> notes = getNotesFromSession(request);
		return new ModelAndView("dashboard-jsp").addObject("username", login).addObject("notes", notes);
	}
	
	@RequestMapping("/addnote")
	public ModelAndView addNote(@RequestParam String noteText, @RequestParam String login) {
		
		List<String> notes = getNotesFromSession(request);
		if (noteText != null && !"".equals(noteText)) {
			notes.add(noteText);
		}
		setNotesInSession(request, notes);
		
		Map<String, Object> modelMap = new HashMap<>();
		modelMap.put("login", login);
		return new ModelAndView("redirect:Dashboard", modelMap);//.addObject("notes", notes).addObject("username", login);
		
	}
	
	@RequestMapping("/deleteAllNotes")
	public ModelAndView deleteNotes(@RequestParam String login) {
		setNotesInSession(request, new ArrayList<String>());
		return new ModelAndView("dashboard-jsp").addObject(getNotesFromSession(request)).addObject("username", login);
	}
	
	@RequestMapping("/manageNotes")
	public ModelAndView manageNotes(@RequestParam String login) {
		List<String> notes = getNotesFromSession(request);
		return new ModelAndView("noteList").addObject("notes", notes).addObject("username", login);
	}
	
	private static List<String> getNotesFromSession(HttpServletRequest request) {
		HttpSession sess = request.getSession(true);
		List<String> notes = (List<String>) sess.getAttribute("NoteList");
		if (notes == null) {
			notes = new ArrayList<String>();
		}
		return notes;
	}
	
	private static void setNotesInSession(HttpServletRequest request, List<String> notes) {
		HttpSession sess = request.getSession(true);
		sess.setAttribute("NoteList", notes);
	}
}
