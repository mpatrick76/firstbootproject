package com.cds.spring.training.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cds.spring.training.domain.MessageObject;

@RestController
public class AjaxController {

	@RequestMapping(value="/ajax/{message}/getMessage")
	public MessageObject ajaxMessage(@PathVariable String message) {
		return new MessageObject(message);
	}
}
