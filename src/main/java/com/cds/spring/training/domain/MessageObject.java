package com.cds.spring.training.domain;

import java.util.Date;

public class MessageObject {

	String message;
	Date timestamp;
	
	public MessageObject(String msg) {
		this.message = msg;
		this.timestamp = new Date();
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

}
