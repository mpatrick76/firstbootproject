FirstBootProject

A very small Spring Boot application that demonstrates the following features:

Supports two templating engines: Groovlets (or Groovy templates), and JSP.
AjaxController demonstrates "automagic" marshalling of POJO into JSON.
Bean configuration is found in the ApplicationRunner class.